package com.ufpr.tads.lol.dao;

import com.ufpr.tads.lol.model.Usuario;
import com.ufpr.tads.lol.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * UsuarioDAO
 */
public class UsuarioDAO {

    public static Usuario login(Usuario u) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Usuario WHERE login = :log AND senha = :sen");
        query.setString("log", u.getEmail());
        query.setString("sen", u.getSenha());
        u = (Usuario)query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return u;
    }

}
