package com.ufpr.tads.lol.dao;

import com.ufpr.tads.lol.model.Cliente;
import com.ufpr.tads.lol.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * ClienteDAO
 */
public class ClienteDAO {

    public static boolean post(Cliente c) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(c);
        session.getTransaction().commit();
        session.close();
        return true;
    }
    
    public static Cliente getByUser(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Cliente WHERE iduser = :id");
        query.setInteger("id", id);
        Cliente c = (Cliente)query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return c;
    }

}
