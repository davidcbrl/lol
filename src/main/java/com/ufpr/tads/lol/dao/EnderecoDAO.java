package com.ufpr.tads.lol.dao;

import com.ufpr.tads.lol.model.Cidade;
import com.ufpr.tads.lol.model.Estado;
import com.ufpr.tads.lol.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * EnderecoDAO
 */
public class EnderecoDAO {

    public static List<Estado> getEstados() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Estado");
        List<Estado> list = new ArrayList<Estado>();
        list = query.list();
        session.close();
        return list;
    }

    public static List<Cidade> getEstadoCidades(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Cidade WHERE idestado = :id");
        query.setInteger("id", id);
        List<Cidade> list = new ArrayList<Cidade>();
        list = query.list();
        session.close();
        return list;
    }

}
