package com.ufpr.tads.lol.dao;

import com.ufpr.tads.lol.model.Pedido;
import com.ufpr.tads.lol.model.Situacao;
import com.ufpr.tads.lol.model.Tipo;
import com.ufpr.tads.lol.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * PedidoDAO
 */
public class PedidoDAO {

    public static boolean post(Pedido p) {
        Session s = null;
        Transaction tx = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            tx = s.beginTransaction();
            s.save(p);
            tx.commit();
            return true;
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return false;
    }

    public static List<Pedido> get() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Pedido");
        List<Pedido> list = new ArrayList<Pedido>();
        list = query.list();
        session.close();
        return list;
    }

    public static List<Tipo> getTipos() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Tipo");
        List<Tipo> list = new ArrayList<Tipo>();
        list = query.list();
        session.close();
        return list;
    }

    public static void cancelar(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("UPDATE Pedido SET idstatus = 6 WHERE id = :id");
        query.setInteger("id", id);
        query.executeUpdate();
        session.close();
        return;
    }

    public static Tipo getTipo(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Tipo WHERE id = :id");
        query.setInteger("id", id);
        Tipo t = (Tipo) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return t;
    }

    public static Situacao getSituacao(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Situacao WHERE id = :id");
        query.setInteger("id", id);
        Situacao s = (Situacao) query.uniqueResult();
        session.getTransaction().commit();
        session.close();
        return s;
    }

}
