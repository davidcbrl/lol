package com.ufpr.tads.lol.mb;

import com.ufpr.tads.lol.facade.AuthFacade;
import com.ufpr.tads.lol.model.Usuario;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.faces.context.FacesContext;

/**
 * Auth
 */
@Named(value = "authMB")
@SessionScoped
public class AuthMB implements Serializable {

    private Usuario usuario = new Usuario();

    public AuthMB() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String login() {
        this.usuario = AuthFacade.login(usuario);
        if (this.usuario != null) {
            if (this.usuario.getCategoria() == 1) {
                return "funcionario/pedidos?faces-redirect=true";
            } else {
                return "cliente/pedidos?faces-redirect=true";
            }
        } else {
            return "index?faces-redirect=true";
        }
    }

    public String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/index?faces-redirect=true";
    }

    public String register() {
        return "cadastro?faces-redirect=true";
    }

    public boolean isAuth() {
        return usuario.getEmail() != null;
    }

}
