package com.ufpr.tads.lol.mb;

import com.ufpr.tads.lol.facade.ClienteFacade;
import com.ufpr.tads.lol.model.Cliente;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Cliente
 */
@RequestScoped
@Named(value="clienteMB")
public class ClienteMB implements Serializable {

    Cliente cliente = new Cliente();

    public ClienteMB() {
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public String cadastrar() {
        if (ClienteFacade.cadastrar(this.cliente)) {
            return "/index?faces-redirect=true";
        } else {
            return "/cadastro?faces-redirect=true";
        }
    }
    
}
