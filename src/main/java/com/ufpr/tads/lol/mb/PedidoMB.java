package com.ufpr.tads.lol.mb;

import com.ufpr.tads.lol.facade.ClienteFacade;
import com.ufpr.tads.lol.facade.PedidoFacade;
import com.ufpr.tads.lol.model.Pedido;
import com.ufpr.tads.lol.model.Roupa;
import com.ufpr.tads.lol.model.Tipo;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Pedido
 */
@SessionScoped
@Named(value = "pedidoMB")
public class PedidoMB implements Serializable {

    private Pedido pedido = new Pedido();
    private Roupa roupaSelecionada = new Roupa();
    private List<Pedido> pedidos = new ArrayList<Pedido>();
    private Pedido pedidoSelecionado = new Pedido();
    private List<Tipo> tipos = new ArrayList<Tipo>();

    @Inject 
    AuthMB authMB;
    
    public PedidoMB() {
    }

    @PostConstruct
    public void init() {
        this.pedidos = PedidoFacade.buscarTodos();
        this.tipos = PedidoFacade.buscarTipos();
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Roupa getRoupaSelecionada() {
        return roupaSelecionada;
    }

    public void setRoupaSelecionada(Roupa roupaSelecionada) {
        this.roupaSelecionada = roupaSelecionada;
    }
    
    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public Pedido getPedidoSelecionado() {
        return pedidoSelecionado;
    }

    public void setPedidoSelecionado(Pedido pedidoSelecionado) {
        this.pedidoSelecionado = pedidoSelecionado;
    }

    public List<Tipo> getTipos() {
        return tipos;
    }

    public void setTipos(List<Tipo> tipos) {
        this.tipos = tipos;
    }
    
    public String cadastrar() {
        this.pedido.setCliente(ClienteFacade.getClienteUsuario(this.authMB.getUsuario().getIdUsuario()));        
        this.pedido.setSituacao(PedidoFacade.buscarSituacao(1));
        PedidoFacade.cadastrar(this.pedido);
        return "pedidos?faces-redirect=true";
    };
        
    public void adicionar() {
        this.roupaSelecionada.setTipo(PedidoFacade.buscarTipo(this.roupaSelecionada.getTipo().getIdTipo()));
        this.pedido.getRoupas().add(this.roupaSelecionada);
        float preco = 0;
        for (Roupa roupa : pedido.getRoupas()) {
            preco = preco + roupa.getTipo().getPreco();
        }
        this.pedido.setPrecoTotal(preco);
        this.roupaSelecionada = new Roupa();
    }

    public void remover(Roupa r) {
        this.pedido.getRoupas().remove(r);
    }
    
    public void cancelar(Integer idPedido) {
        PedidoFacade.cancelar(idPedido);
    }

}
