package com.ufpr.tads.lol.mb;

import com.ufpr.tads.lol.facade.EnderecoFacade;
import com.ufpr.tads.lol.model.Cidade;
import com.ufpr.tads.lol.model.Endereco;
import com.ufpr.tads.lol.model.Estado;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

/**
 * Endereco
 */
@SessionScoped
@Named(value="enderecoMB")
public class EnderecoMB implements Serializable {

    private Endereco endereco = new Endereco();
    private List<Estado> estados = new ArrayList<Estado>();
    private List<Cidade> cidades = new ArrayList<Cidade>();
    
    /**
     * Creates a new instance of EnderecoMB
     */
    public EnderecoMB() {
    }
    
    @PostConstruct
    public void init() {
        this.estados = EnderecoFacade.buscarEstados();
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Estado> getEstados() {
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }
    
    public void buscarEstadoCidades(Integer idEstado) {
        this.cidades = EnderecoFacade.buscarEstadoCidades(idEstado);
    }
    
}
