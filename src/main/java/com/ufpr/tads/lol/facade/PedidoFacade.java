package com.ufpr.tads.lol.facade;

import com.ufpr.tads.lol.dao.PedidoDAO;
import com.ufpr.tads.lol.model.Pedido;
import com.ufpr.tads.lol.model.Situacao;
import com.ufpr.tads.lol.model.Tipo;
import java.util.List;

/**
 * PedidoFacade
 */
public class PedidoFacade {

    public static boolean cadastrar(Pedido p) {
        return PedidoDAO.post(p);
    }
    
    public static List<Pedido> buscarTodos() {
        return PedidoDAO.get();
    }
    
    public static List<Tipo> buscarTipos() {
        return PedidoDAO.getTipos();
    }
    
    public static void cancelar(Integer id) {
        PedidoDAO.cancelar(id);
    }
    
    public static Tipo buscarTipo(Integer id) {
        return PedidoDAO.getTipo(id);
    }

    public static Situacao buscarSituacao(Integer id) {
        return PedidoDAO.getSituacao(id);
    }

}
