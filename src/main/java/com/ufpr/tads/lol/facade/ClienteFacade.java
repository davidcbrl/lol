package com.ufpr.tads.lol.facade;

import com.ufpr.tads.lol.dao.ClienteDAO;
import com.ufpr.tads.lol.model.Cliente;

/**
 * ClienteFacade
 */
public class ClienteFacade {
    
    public static boolean cadastrar(Cliente c) {
        return ClienteDAO.post(c);
    }
    
    public static Cliente getClienteUsuario(Integer id) {
        return ClienteDAO.getByUser(id);
    }

}
