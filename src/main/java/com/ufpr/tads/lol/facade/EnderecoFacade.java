package com.ufpr.tads.lol.facade;

import com.ufpr.tads.lol.dao.EnderecoDAO;
import com.ufpr.tads.lol.model.Cidade;
import com.ufpr.tads.lol.model.Estado;
import java.util.List;

/**
 * EnderecoFacade
 */
public class EnderecoFacade {

    public static List<Estado> buscarEstados() {
        return EnderecoDAO.getEstados();
    }
    
    public static List<Cidade> buscarEstadoCidades(Integer id) {
        return EnderecoDAO.getEstadoCidades(id);
    }
    
}
