package com.ufpr.tads.lol.facade;

import com.ufpr.tads.lol.dao.UsuarioDAO;
import com.ufpr.tads.lol.model.Usuario;

/**
 * AuthFacade
 */
public class AuthFacade {

    public static Usuario login(Usuario u) {
        return UsuarioDAO.login(u);
    }

}
