package com.ufpr.tads.lol.util;

import com.ufpr.tads.lol.mb.AuthMB;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 * CheckAuth
 */
public class CheckAuth implements PhaseListener {

    private static final long serialVersionUID = 1;

    public void beforePhase(PhaseEvent event) {
    }

    public void afterPhase(PhaseEvent event) {
        FacesContext context = event.getFacesContext();
        if ("/index.xhtml".equals(context.getViewRoot().getViewId())) {
            return;
        }
        AuthMB authMB = context.getApplication().evaluateExpressionGet(context, "#{authMB}", AuthMB.class);
        if (!authMB.isAuth()) {
            NavigationHandler handler = context.getApplication().getNavigationHandler();
            handler.handleNavigation(context, null, "index?faces-redirect=true");
            context.renderResponse();
        }
    }

    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

}
