package com.ufpr.tads.lol.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * Pedido
 */
@Entity
@Table(name = "tb_pedido")
public class Pedido {

    private Integer idPedido;
    private Float precoTotal;
    private Date prazoTotal;
    private Cliente cliente;
    private Situacao situacao;
    private List<Roupa> roupas;

    public Pedido() {
        this.roupas = new ArrayList<Roupa>();
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Integer idPedido) {
        this.idPedido = idPedido;
    }

    @Column(name = "preco_total")
    public Float getPrecoTotal() {
        return precoTotal;
    }

    public void setPrecoTotal(Float precoTotal) {
        this.precoTotal = precoTotal;
    }

    @Transient
    @Temporal(TemporalType.DATE)
    @Column(name = "data_final")
    public Date getPrazoTotal() {
        return prazoTotal;
    }

    public void setPrazoTotal(Date prazoTotal) {
        this.prazoTotal = prazoTotal;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idcliente", updatable = true)
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idstatus", updatable = true)
    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    @ManyToMany(
            targetEntity = com.ufpr.tads.lol.model.Roupa.class,
            cascade = {CascadeType.ALL},
            fetch = FetchType.EAGER)
    @JoinTable(
            name = "tb_roupa_pedido",
            joinColumns = {@JoinColumn(name = "idpedido")},
            inverseJoinColumns = {@JoinColumn(name = "idroupa")})
    public List<Roupa> getRoupas() {
        return roupas;
    }

    public void setRoupas(List<Roupa> roupas) {
        this.roupas = roupas;
    }

}
