package com.ufpr.tads.lol.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Usuario
 */
@Entity
@Table(name="tb_usuario")
public class Usuario {

    private Integer idUsuario;
    private String email;
    private String senha;
    private Integer categoria;

    public Usuario() {
    }
    
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Column(name="login")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @Column(name="senha")
    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Column(name="idrole")
    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    @Transient
    public boolean isFuncionario() {
        return this.categoria == 1;
    } 

    @Transient
    public boolean isCliente() {
        return this.categoria == 2;
    } 
}
