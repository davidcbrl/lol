package com.ufpr.tads.lol.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Roupa
 */
@Entity
@Table(name = "tb_roupa")
public class Roupa {

    private Integer idRoupa;
    private String nome;
    private Tipo tipo;
    private List<Pedido> pedidos;

    public Roupa() {
        this.tipo = new Tipo();
        this.pedidos = new ArrayList<Pedido>();
    }

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getIdRoupa() {
        return idRoupa;
    }

    public void setIdRoupa(Integer idRoupa) {
        this.idRoupa = idRoupa;
    }

    @Column(name = "nome")
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "idtipo", updatable = true)
    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    @ManyToMany(
            cascade={CascadeType.ALL},
            mappedBy="roupas",
            fetch=FetchType.EAGER)
    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

}
