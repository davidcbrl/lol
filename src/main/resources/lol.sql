-- drop database if exists lol; 
create database if not exists lol;
use lol;

drop table if exists tb_roupa_pedido;
drop table if exists tb_roupa;
drop table if exists tb_tipo;
drop table if exists tb_pedido;
drop table if exists tb_estatus;
drop table if exists tb_cliente;
drop table if exists tb_endereco;
drop table if exists tb_funcionario;
drop table if exists tb_usuario;

create table if not exists tb_role(
id int primary key auto_increment,
nome varchar(255)
);

create table if not exists tb_usuario
(
id int primary key auto_increment,
login varchar(255),
senha varchar(255),  
idPerson int null,
idrole int not null,
constraint FkRoleUsuario foreign key (idrole) references tb_role(id),
constraint UkUsuario unique (login,idrole)
);

create table if not exists tb_funcionario (
id int primary key auto_increment,
matricula int unique not null,
nome varchar(255) not null,
email varchar(255) unique not null,
nascimento date not null,
iduser int null,
constraint FkUsuarioFuncionario foreign key (iduser) references tb_usuario(id)
);
drop trigger tgAddUsuarioFuncionario
delimiter |
CREATE TRIGGER tgAddUsuarioFuncionario before INSERT ON tb_funcionario
  FOR EACH ROW
  BEGIN	
    if((select max(id)from tb_funcionario) >= 1) then
    INSERT INTO tb_usuario (login,senha,idrole,idperson) values (NEW.matricula, concat(NEW.nome,NEW.matricula),1,(select max(id)from tb_funcionario)+1);
    else
        INSERT INTO tb_usuario (login,senha,idrole,idperson) values (NEW.matricula, concat(NEW.nome,NEW.matricula),1,1);
    end if;
    
	set NEW.iduser = (select max(id) from tb_usuario);
  END;
|
delimiter ;

create table if not exists tb_endereco(
id int primary key auto_increment,
logradouro varchar(255) not null,
numero varchar(7)not null,
bairro varchar(255) not null,
cep varchar(255) not null,
idcidade int not null,
constraint FkCidadeEndereco foreign key (idcidade) references tb_cidade(id),
idestado int not null,
constraint FkEstadoEndereco foreign key (idestado) references tb_estado(id)
);

create table if not exists tb_cliente(
id int primary key auto_increment,
nome varchar(255) not null,
sexo char(2)not null,
constraint CkSexo check (sexo in ('f','F','m','M')),
idendereco int null,
constraint FkEnderecoCliente foreign key (idendereco) references tb_endereco(id),
cpf varchar(11) unique not null,
email varchar(255)unique not null,
telefone varchar(13)null,
iduser int null
);
select * from tb_usuario
drop trigger tgAddUsuarioCliente
delimiter |
CREATE TRIGGER tgAddUsuarioCliente before INSERT ON tb_cliente
  FOR EACH ROW
  BEGIN
	if((select max(id)from tb_cliente) >= 1) then
        INSERT INTO tb_usuario (login,senha,idrole,idperson) values (NEW.email, NEW.cpf,2,(select max(id)from tb_cliente)+1);
    else
    INSERT INTO tb_usuario (login,senha,idrole,idperson) values (NEW.email, NEW.cpf,2,1);
	end if;
    set NEW.iduser = (select max(id) from tb_usuario);
  END;
|
delimiter ;


create table if not exists tb_estatus(
id int primary key auto_increment,
nome varchar(255)not null
);

create table if not exists tb_pedido(
id int primary key auto_increment,
idcliente int not null,
constraint FkClientePedido foreign key (idcliente) references tb_cliente(id),
idstatus int not null,
constraint FkEstatusPedido foreign key (idstatus) references tb_estatus(id),
preco_total double null,
data_final date
);

create table if not exists tb_tipo(
id int primary key auto_increment,
nome varchar(255) not null,
preco double not null,
dias int not null
);

create table if not exists tb_roupa(
id int primary key auto_increment,
nome varchar(255)not null,
idtipo int not null,
constraint FkTipoRoupa foreign key (idtipo) references tb_tipo(id)
);

create table if not exists tb_roupa_pedido(
idroupa int not null,
constraint FkRoupaRoupa_Pedido foreign key (idroupa) references tb_roupa(id),
idpedido int not null,
constraint FkPedidoRoupa_Pedido foreign key (idpedido) references tb_pedido(id),
qtd int null
);
select * from tb_usuario
drop trigger tgAddPrecoTotalDataFinalPedido
delimiter |
CREATE TRIGGER tgAddPrecoTotalDataFinalPedido after INSERT ON tb_roupa_pedido
  FOR EACH ROW
  BEGIN
  declare Pedidoid int;
  declare datamaior int;
  select NEW.idpedido into Pedidoid;
    update tb_pedido set preco_total = 
    (select sum(t.preco*rp.qtd) from tb_tipo t, tb_roupa_pedido rp,tb_roupa r 
    where rp.idroupa = r.id and rp.idpedido = Pedidoid and r.idtipo = t.id),
    data_final = (date_add(curdate(),interval (select max(t.dias) from tb_roupa_pedido rp, tb_tipo t, tb_roupa r 
    where rp.idpedido = Pedidoid and rp.idroupa = r.id and r.idtipo = t.id) DAY))
    where id = Pedidoid;
  END;
|
delimiter ;


insert into tb_tipo(nome,preco,dias) values
('lycra',15.00,3),
('moletom',20,5),
('poliester',17.50,4),
('algodao',13.75,2),
('algodao pesado',75.00,7),
('elaste',50.00,3),
('jeans',10.00,2);

INSERT INTO tb_roupa (nome,idtipo) VALUES 
("regata","1"),
("calça","5"),
("shorts","3"),
("camisa","2"),
("shortinho","2"),
("shorts","1"),
("camisa","2"),
("calça","4"),
("shortinho","6"),
("shortinho","7");

insert into tb_endereco (logradouro,numero,bairro,cep,idcidade,idestado) values 
('rua1','12345','bairro1','cep1',1,1),
('rua2','14345','bairro2','cep2',107,2),
('rua3','15345','bairro3','cep3',767,6),
('rua4','12365','bairro4','cep4',882,7),
('rua5','12325','bairro5','cep5',886,9),
('rua6','13345','bairro6','cep6',1506,12),
('rua7','12315','bairro7','cep7',1619,11),
('rua8','10345','bairro8','cep8',2486,14),
('rua9','13345','bairro9','cep9',3806,20),
('rua10','16345','bairro10','cep10',3863,23);

insert into tb_role(nome)values ('funcionario'),('cliente');

insert into tb_cliente(nome,sexo,idendereco,cpf,email,telefone)values 
('rhonner lindo','m',2,'11111111111','rico@rico.com','(41)997503929'),
('cliente2','m',3,'2222222222','rico@rico2.com','(41)997103929'),
('cliente3','m',4,'33333333333','rico@rico3.com','(41)992503929'),
('cliente4','m',5,'44444444444','rico@rico4.com','(41)997303929'),
('daniel burgues','m',6,'55555555555','burgues@safado.com','(41)997403929'),
('david burgues','m',7,'66666666666','burgues@humilde.com','(41)997563929'),
('cliente7','m',8,'77777777777','rico@rico7.com','(41)997583929'),
('cliente8','m',9,'88888888888','rico@rico8.com','(41)997503929'),
('cliente9','m',10,'99999999999','rico@rico9.com','(41)997500929'),
('cliente10','m',1,'01010101010','rico@rico10.com','(41)997203929');
insert into tb_funcionario (matricula,nome,email,nascimento)values
(1113,'funcionario3','func3@func3.com','2019-06-15'),
(1114,'funcionario4','func4@func4.com','2019-06-15'),
(1115,'funcionario5','func5@func5.com','2019-06-15'),
(1116,'funcionario6','func6@func6.com','2019-06-15'),
(1117,'funcionario7','func7@func7.com','2019-06-15'),
(1118,'funcionario8','func8@func8.com','2019-06-15'),
(1119,'funcionario9','func9@func9.com','2019-06-15'),
(1120,'funcionario10','func10@func10.com','2019-06-15');

insert into tb_estatus(nome) values ('Aberto'),('Lavando'), ('Resolvido'), ('Pago'), ('Entregando'), ('Fechado');

insert into tb_pedido(idcliente,idstatus)values
(1,1),
(2,1),
(3,1),
(4,1),
(5,1),
(6,1),
(7,1),
(8,1),
(9,1),
(10,1);
insert into tb_roupa_pedido values (1,1,1),(2,2,2),(3,3,3),(4,4,4),(5,5,5),(6,6,6),(7,7,7),(8,8,8),(9,9,9),(10,10,10);
insert into tb_roupa_pedido values (10,1,1),(9,2,2),(8,3,3),(7,4,4),(6,5,5),(5,6,6),(4,7,7),(3,8,8),(2,9,9),(1,10,10);